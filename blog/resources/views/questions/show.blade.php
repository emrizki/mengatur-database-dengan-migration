@extends('adminlte.master')

@section('content')
  <div class="m-3">
    <h1>{{ $question->title }}</h1>
    <p>{{ $question->body }}</p>
  </div>
@endsection