@extends('adminlte.master')

@section('content')
  <div class="card card-info m-4">
    <div class="card-header">
      <h3 class="card-title">Create Question</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form class="form-horizontal" action="/questions" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group row">
          <label for="title" class="col-sm-2 col-form-label">Judul:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="title" value="{{old('title', '')}}"name="title" placeholder="Title">
          @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          </div>
        </div>
        <div class="form-group row">
          <label for="body" class="col-sm-2 col-form-label">Body:</label>
          <div class="col-sm-10">
            <textarea name="body" id="isi" cols="30" rows="10">{{old('body', '')}}</textarea>
            <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="Password"> -->
          @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          </div>
        </div>
        <!-- <div class="form-group row"> -->
          <!-- <div class="offset-sm-2 col-sm-10">
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck2">
              <label class="form-check-label" for="exampleCheck2">Remember me</label>
            </div>
          </div> -->
          <!-- <label for="tanggal_dibuat" class="col-sm-2 col-form-label">Tanggal Dibuat</label>
          <div class="col-sm-10">
            <input type="date" name="tanggal_dibuat" id="tanggal_dibuat">
          </div> -->
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-info">Create</button>
        <!-- <button type="submit" class="btn btn-default float-right">Cancel</button> -->
      </div>
      <!-- /.card-footer -->
    </form>
  </div>
@endsection