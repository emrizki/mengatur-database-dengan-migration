@extends('adminlte.master')

@section('content')
<div class="card m-4">
  <div class="card-header">
    <h3 class="card-title">Questions</h3>
  </div>
  <!-- /.card-header -->
    <div class="card-body p-0">
    @if(session('success'))
      <div class="alert alert-success m-3">
        {{ session('success')}}
      </div>
    @endif
    <a href="/questions/create" class="btn btn-info ml-3 mt-3">Add Question</a>
      <table class="table table-condensed">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Title</th>
            <th>Body</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        @forelse($questions as $key => $question)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $question->title }}</td>
            <td>{{ $question->body }}</td>
            <td>
              <a href="/questions/{{$question->id}}" class="btn btn-warning">Show</a> |
              <a href="/questions/{{$question->id}}/edit" class="btn btn-primary">Edit</a> |
              <form action="/questions/{{$question->id}}" method="POST" style="display: inline;">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
              <!-- <a href="/questions/{{$question->id}}" class="btn btn-danger">Delete</a> -->
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="4" style="text-align: center">No Questions</td>
          </tr>
        @endforelse
          <!-- <tr>
            <td>2.</td>
            <td>Clean database</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar bg-warning" style="width: 70%"></div>
              </div>
            </td>
            <td><span class="badge bg-warning">70%</span></td>
          </tr> -->
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection