<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class QuestionController extends Controller
{
    public function create() {
        return view('questions.create');
    }
    public function store(Request $request) {
        // dd($request->all());

        $request->validate([
            'title' => 'required|unique:questions',
            'body' => 'required'
        ]);
        $query = DB::table('questions')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);

        return redirect('/questions')->with('success', 'Success Add Question!');
    }

    public function index() {
        $questions = DB::table('questions')->get();
        // dd($questions);
        return view('questions.index', compact('questions'));
    }

    public function show($id) {
        $question = DB::table('questions')->where('id', $id)->first();
        // dd($question);
        return view('questions.show', compact('question'));
    }

    public function edit($id) {
        $question = DB::table('questions')->where('id', $id)->first();

        return view('questions.edit', compact('question'));
    }

    public function update($id, Request $request) {
        $query = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'],
                        'body' => $request['body']
                    ]);
        return redirect('/questions')->with('success', 'Update Success!');
    }

    public function destroy($id) {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/questions')->with('success', 'Delete Success!');
    }
}
